/*
* Copyright (c) 2015 Razeware LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

import Foundation

enum UserValidationState {
  case Valid
  case Invalid(String)
}

class UserViewModel {
  private let minUsernameLength = 4
  private let minPasswordLength = 5
  private let codeRefreshTime = 5.0

  var username: String?
  var password: String?
  var accessCode: Box<String?> = Box(nil)

  var protectedUsername: String {
    guard let username = username else {
      return ""
    }

    let characters = username.characters

    if characters.count >= minUsernameLength {
      var displayName = [Character]()
      for (index, character) in characters.enumerate() {
        if index > characters.count - minUsernameLength {
          displayName.append(character)
        } else {
          displayName.append("*")
        }
      }
      return String(displayName)
    } else {
      return username
    }
  }

  init() {
    startAccessCodeTimer()
  }
}

// MARK: Public Methods
extension UserViewModel {
  func validate() -> UserValidationState {
    guard let username = username, password = password else {
      return .Invalid("Username and password are required.")
    }

    if username.isEmpty || password.isEmpty {
      return .Invalid("Username and password are required.")
    }

    if username.characters.count < minUsernameLength {
      return .Invalid("Username needs to be at least \(minUsernameLength) characters long.")
    }

    if password.characters.count < minPasswordLength {
      return .Invalid("Password needs to be at least \(minPasswordLength) characters long.")
    }

    return .Valid
  }

  func login(completion: (errorString: String?) -> Void) {
    guard let username = username, password = password else {
      completion(errorString: "Username and password are required.")
      return
    }

    LoginService.loginWithUsername(username, password: password) { success in
      if success {
        completion(errorString: nil)
      } else {
        completion(errorString: "Invalid credentials.")
      }
    }
  }
}

// MARK: - Private Methods
private extension UserViewModel {
  func startAccessCodeTimer() {
    accessCode.value = LoginService.generateAccessCode()

    let dispatchTime = dispatch_time(DISPATCH_TIME_NOW, Int64(codeRefreshTime * Double(NSEC_PER_SEC)))
    dispatch_after(dispatchTime, dispatch_get_main_queue()) { [weak self] in
      self?.startAccessCodeTimer()
    }
  }
}
